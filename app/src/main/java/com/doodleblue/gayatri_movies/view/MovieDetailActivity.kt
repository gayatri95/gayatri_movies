package com.doodleblue.gayatri_movies.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.doodleblue.gayatri_movies.R
import com.doodleblue.gayatri_movies.common.Constants
import com.doodleblue.gayatri_movies.databinding.ActivityMovieDetailBinding
import com.doodleblue.gayatri_movies.model.MovieDetail
import kotlinx.android.synthetic.main.activity_movie_detail.*


class MovieDetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMovieDetailBinding
    private var movieDetail: MovieDetail? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_movie_detail)

        if (intent.hasExtra(Constants.MOVIE_DATA)) {
            movieDetail = intent.extras?.get(Constants.MOVIE_DATA) as MovieDetail
        }


    }

    override fun onResume() {
        super.onResume()

        binding.data = movieDetail

        Glide.with(this)
            .load(movieDetail?.multimedia?.src)
            .centerCrop()
            .placeholder(R.mipmap.ic_launcher_round)
            .into(ivMovie)

    }

}