package com.doodleblue.gayatri_movies.repository

import com.doodleblue.gayatri_movies.network.RetrofitService

class MoviesRepository(private val retrofitService: RetrofitService) {

    suspend fun getAllMovies(apiKey: String) = retrofitService.getAllMovies(apiKey)

}