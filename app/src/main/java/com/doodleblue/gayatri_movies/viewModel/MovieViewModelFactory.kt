package com.doodleblue.gayatri_movies.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.doodleblue.gayatri_movies.repository.MoviesRepository

class MovieViewModelFactory (private val repository: MoviesRepository): ViewModelProvider.Factory{

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(MoviesListViewModel::class.java)) {
            MoviesListViewModel(this.repository) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }

}