package com.doodleblue.gayatri_movies.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.doodleblue.gayatri_movies.model.MovieDetail
import com.doodleblue.gayatri_movies.repository.MoviesRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MoviesListViewModel(private val moviesRepository: MoviesRepository) : ViewModel() {

    val errorMessage = MutableLiveData<String>()
    val movieList = MutableLiveData<MutableList<MovieDetail>>()
    val loading = MutableLiveData<Boolean>()

    fun getAllMovies(apiKey: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = moviesRepository.getAllMovies(apiKey)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    val movieListResponse = response.body()
                    movieList.postValue(movieListResponse?.results)
                    loading.value = false
                } else {
                    onError("Error : ${response.message()} ")
                }
            }
        }
    }

    fun testMovieAPI(apiKey: String) : String {
        var status = ""
        CoroutineScope(Dispatchers.IO).launch {
            val response = moviesRepository.getAllMovies(apiKey)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    status = "Success"
                } else {
                    status = "Fail"
                    onError("Error : ${response.message()} ")
                }
            }
        }

        return  status
    }

    private fun onError(message: String) {
        errorMessage.value = message
        loading.value = false
    }


}