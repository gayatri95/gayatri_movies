package com.doodleblue.gayatri_movies

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.doodleblue.gayatri_movies.model.MovieDetail
import com.doodleblue.gayatri_movies.model.MoviesListResponse
import com.doodleblue.gayatri_movies.network.RetrofitService
import com.doodleblue.gayatri_movies.repository.MoviesRepository
import com.doodleblue.gayatri_movies.viewModel.MoviesListViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.*
import org.junit.Assert.assertNotNull
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response


@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class MovieViewModelTest {

    // A JUnit Test Rule that swaps the background executor used by
    // the Architecture Components with a different one which executes each task synchronously.
    // You can use this rule for your host side tests that use Architecture Components.
    @Rule
    @JvmField
    var rule = InstantTaskExecutorRule()

    // Test rule for making the RxJava to run synchronously in unit test
    companion object {
        @ClassRule
        @JvmField
        val schedulers = TestCoroutineRule()
    }

    @Mock
    lateinit var apiService: RetrofitService

    @Mock
    lateinit var moviesRepository: MoviesRepository

    @Mock
    lateinit var retroRXViewModel: MoviesListViewModel


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        moviesRepository = MoviesRepository(apiService)
        retroRXViewModel = MoviesListViewModel(moviesRepository)
    }


    @ExperimentalCoroutinesApi
    @Test
    fun checkViewModelSuccessTest() = runBlockingTest {
        val appId = "kxU2GXge4akcQztFctrqi6FqR8pyT6dV"
        var retroRxModel = MovieDetail("tile", 1, "1", display_title = "Frozen")
        var retroRXModelList = ArrayList<MovieDetail>()
        retroRXModelList.add(retroRxModel)
        val response = Response.success(
            MoviesListResponse(
                copyright = " ",
                has_more = false,
                num_results = 0,
                results = retroRXModelList,
                status = ""
            )
        )
        Mockito.`when`(apiService.getAllMovies(appId)).thenReturn(response)
        val result = retroRXViewModel.testMovieAPI(appId)

        Assert.assertEquals("Success", result)
        Mockito.verify(moviesRepository, Mockito.times(1)).getAllMovies(appId)
        Mockito.verifyNoMoreInteractions(moviesRepository)
    }


    @Test
    fun testNull() = runBlockingTest {
        val appId = "kxU2GXge4akcQztFctrqi6FqR8pyT6dV"
        Mockito.`when`(apiService.getAllMovies(appId)).thenReturn(null)
        assertNotNull(retroRXViewModel.getAllMovies(appId))
    }

    @Test
    fun testApiFetchDataSuccess() = runBlockingTest {
        // Mock API response
        val appId = "kxU2GXge4akcQztFctrqi6FqR8pyT6dV"
        var retroRxModel = MovieDetail("tile", 1, "1", display_title = "Frozen")
        var retroRXModelList = ArrayList<MovieDetail>()
        retroRXModelList.add(retroRxModel)
        val response = Response.success(
            MoviesListResponse(
                copyright = " ",
                has_more = false,
                num_results = 0,
                results = retroRXModelList,
                status = ""
            )
        )
        `when`(apiService.getAllMovies(appId)).thenReturn(response)
        retroRXViewModel.getAllMovies(appId)
//        verify(retroRXViewModel, Mockito.times(1)).getAllMovies(appId)
    }


}